# Pré-requisitos

- docker
- docker-composer



# Como fazer o build e executar o servidor?

- Você pode executar `docker-compose build` para fazer o build
 
- Depois, você poderá executar `docker-compose run web` para executar o servidor

- Você pode alterar no arquivo [.env](.env) o secret e os dados de conexão com o banco de dados  

- Aproveite e ajuste os endereços no arquivo [Makefile](Makefile)

- Antes de utilizar o sistema, com os serviços rodando, efetue a criação da tabela de tenantes com o comando `make migration`

- Existe um endpoint para cadastro de novos tenants e umn exemplo está disponível com o comando `make tenant` 



# Como executar a importação dos dados?

- Da mesma forma como estava nas instruções, bastando substituir o endereço IP do serviço
```
curl -X POST \
  http://{$FLASK_CONTAINER_IP}:5000/ \
  -H 'cache-control: no-cache' \
  -H 'content-type: multipart/form-data' \
  -F data=@pdv_list.csv \
  -F tenant=<some-tenant-id>
```

- Nos meus testes não consumiu mais do que 38 MB de memória para o arquivo de exemplo.


![memory](mem.png)

- Importante ressaltar que o commit da operação de inclusão só ocorre no final da mesma, de modo a garantir que o arquivo seja importado em sua totalidade ou seja completamente descartado (para ser importado depois).

- O arquivo [Makefile](Makefile) contém o comando `make test` que deve executar a request com os dados de exemplo.


# Como visualizar os dados?

- Existe um endereço utilizando o método GET para visualizar os dados:

```
curl http://{$FLASK_CONTAINER_IP}:5000/<tenant>?offset=100
```

- O tenant precisa ser enviado no endereço do recurso

- O offset pode ser enviado como filtro na `query string`

- O arquivo [Makefile](Makefile) contém o comando `make test` que deve executar a request com os dados de exemplo.



# Sobre a funcionalidade de criação de tenants

- Como o projeto é pequeno, não se fez uso de migrations.

- Ela não estava no escopo do projeto, então o mais adequado é separará-lo da funcionalidade de envio de dados dos PDVs.

- Dois tenants diferentes podem ter o mesmo nome de banco de dados, se existirem, por exemplo, em servidores de banco de dados diferentes.

-- A criação de um novo tenant não verifica se o banco de dados e/ou a tabela desejada já existe. 



# Considerações finais

- Desenvolvi focando na performance da aplicação

- Desenvolvi dois testes automatizados para as regras de validação de CPF e nomes de PDV e cidade (Dentro do container `python -m unittest -v tests/test_business.py`) mas poderiam ser feitos testes para os endpoints e para a operação de salvar os dados em banco

- A criptografia dos dados de conexão pode ser facilmente feita utilizando a biblioteca `cryptography` que eu cheguei a adicionar nas dependências do projeto

