import os
import re
import csv
import json

from flask import Flask, request
import psycopg2

APP_TENANTS_CONN = os.environ['APP_TENANTS_CONN']
APP_DATABASE_SECRET = os.environ['APP_DATABASE_SECRET']

app = Flask(__name__)
zip_code_regex = re.compile(r'^\d{8}$')


@app.route('/<tenant>', methods=['GET'])
def pdv_list(tenant):
    tenant_data_source = get_tenant_data_source_by_tenant_name(tenant)

    if tenant_data_source is None:
        return '', 404

    offset = request.args.get("offset", 0)
    conn = None
    result = []

    try:
        conn = psycopg2.connect(host=tenant_data_source['host'].strip(),
                                user=tenant_data_source['user'].strip(),
                                password=tenant_data_source['password'].strip(),
                                dbname=tenant_data_source['dbname'].strip())
        cursor = conn.cursor()
        cursor.execute("SELECT id, name, city, address, zip_code FROM pdv LIMIT 100 OFFSET %s", (offset,))
        rows = cursor.fetchall()
        for row in rows:
            result.append({"name": row[0], "city": row[1], "address": row[2], "zip_code": row[3]})

        cursor.close()

    except (Exception, psycopg2.DatabaseError) as error:
        raise error
        return "", 500
    finally:
        if conn is not None:
            conn.close()

    return json.dumps({"count": 0, "data": result}), 200


@app.route('/_migrations', methods=['POST'])
def migrations():
    table_query = """
    CREATE TABLE tenants
    (
      identifier character(511) NOT NULL,
      data_source text,
      CONSTRAINT tenants_pkey PRIMARY KEY (identifier)
    )
    """

    conn = None
    try:
        conn = psycopg2.connect(APP_TENANTS_CONN)
        conn.autocommit = True
        cur = conn.cursor()
        cur.execute(table_query)
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        return "", 500
    finally:
        if conn is not None:
            conn.close()

    return "", 201


@app.route('/', methods=['POST'])
def pdv_insert():
    tenant = request.form['tenant']
    input_iterator = request.files['data']
    tenant_data_source = get_tenant_data_source_by_tenant_name(tenant)
    pdv_persist(input_iterator, tenant_data_source)

    return "", 201


@app.route('/tenant', methods=['GET'])
def tenant_list():
    return 'List Tenants'


@app.route('/tenant', methods=['POST'])
def tenant_insert():
    tenant = request.form['tenant']
    if not tenant.strip():
        return "You have to inform a Tenant name\n", 400

    tenant_data_source = request.form['tenant_data_source']
    if not tenant_data_source.strip():
        return "You have to inform a Tenant Data Source\n", 400

    tenant_data_source_in_db = has_tenant_data_source_by_tenant_name(tenant)

    if tenant_data_source_in_db:
        return 'Tenant "{:s}" already exists\n'.format(tenant), 400

    code = tenant_persist(tenant, tenant_data_source)

    return "", code


def tenant_persist(tenant_name, tenant_data_source):
    insert_query = "INSERT INTO tenants VALUES (%s, %s)"
    table_query = """
    CREATE TABLE pdv
    (
      id serial NOT NULL,
      name text,
      city text,
      address text,
      zip_code character(8),
      CONSTRAINT pdv_pkey PRIMARY KEY (id)
    )
    WITH (
      OIDS=FALSE
    );
    """

    tenant_data_source_dict = json.loads(tenant_data_source)

    if 'dbname' not in tenant_data_source_dict or not tenant_data_source_dict['dbname'].strip():
        return 400

    if 'user' not in tenant_data_source_dict or not tenant_data_source_dict['user'].strip():
        return 400

    if 'password' not in tenant_data_source_dict or not tenant_data_source_dict['password'].strip():
        return 400

    if 'host' not in tenant_data_source_dict or not tenant_data_source_dict['host'].strip():
        return 400

    conn1 = None
    conn2 = None
    try:
        conn1 = psycopg2.connect(APP_TENANTS_CONN)
        conn1.autocommit = True

        #create database
        cur = conn1.cursor()
        create_database = "CREATE DATABASE \"{:s}\"".format(tenant_data_source_dict['dbname'],)
        cur.execute(create_database)
        cur.close()

        # Insert data in "tenants" table
        cur = conn1.cursor()
        cur.execute(insert_query, (tenant_name, tenant_data_source))
        cur.close()

        # create table
        conn2 = psycopg2.connect(host=tenant_data_source_dict['host'].strip(),
                                 user=tenant_data_source_dict['user'].strip(),
                                 password=tenant_data_source_dict['password'].strip(),
                                 dbname=tenant_data_source_dict['dbname'].strip())
        conn2.autocommit = True
        cur = conn2.cursor()
        cur.execute(table_query)
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        return 500
    finally:
        if conn1 is not None:
            conn1.close()
        if conn2 is not None:
            conn2.close()
    return 201


def has_tenant_data_source_by_tenant_name(tenant_name):
    conn = None
    data_source = None
    try:
        conn = psycopg2.connect(APP_TENANTS_CONN)
        cur = conn.cursor()
        cur.execute("SELECT data_source FROM tenants WHERE identifier = %s", (tenant_name,))
        row = cur.fetchone()
        cur.close()

        if row is not None:
            return True

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return data_source


def get_tenant_data_source_by_tenant_name(tenant_name):
    if tenant_name is None:
        return None

    conn = None
    data_source = None
    try:
        conn = psycopg2.connect(APP_TENANTS_CONN)
        cur = conn.cursor()
        cur.execute("SELECT data_source FROM tenants WHERE identifier = %s", (tenant_name,))
        row = cur.fetchone()

        if row is not None:
            data_source = json.loads(row[0])

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return data_source


def decode_utf8(input_iterator):
    for l in input_iterator:
        yield l.decode('utf-8')


def pdv_persist(input_iterator, tenant_data_source):
    if tenant_data_source is None:
        return

    conn = None
    try:
        conn = psycopg2.connect(host=tenant_data_source['host'].strip(),
                                user=tenant_data_source['user'].strip(),
                                password=tenant_data_source['password'].strip(),
                                dbname=tenant_data_source['dbname'].strip())
        conn.autocommit = False
        cur = conn.cursor()
        reader = csv.DictReader(decode_utf8(input_iterator))
        for row in reader:
            cur.execute(
                'INSERT INTO pdv ("name", "city", "address", "zip_code") VALUES (%s, %s, %s, %s)', 
                (check_empty(row['nome']), check_empty(row['cidade']), row['endereco'], check_zip_code(row['cep']),)
            )
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def check_zip_code(zip_code):
    if zip_code_regex.match(zip_code):
        return zip_code
    return "00000000"


def check_empty(data):
    if data.strip():
        return data.strip()
    return "NOME DESCONHECIDO"
