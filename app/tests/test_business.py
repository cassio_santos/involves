import unittest
from app import check_zip_code, check_empty


class TestBusinessMethods(unittest.TestCase):
    def test_check_zip_code_invalid_cases(self):
        data = [
            '', 
            '"', 
            ' ', 
            '123456789',
            '12345-678',
        ]
        for item in data: 
            self.assertEqual(check_zip_code(item), '00000000')

    def test_check_zip_code_valid_cases(self):
        data = [
            '00000000', 
            '25874698', 
            '99999999', 
            '12345678', 
        ]
        for item in data: 
            self.assertEqual(check_zip_code(item), item)

    def test_check_empty_invalid_cases(self):
        data = [
            '', 
            '\t', 
            '   ', 
            '\n',
        ]
        for item in data: 
            self.assertEqual(check_empty(item), 'NOME DESCONHECIDO')

    def test_check_empty_valid_cases(self):
        data = [
            '00000000', 
            '25874698', 
            '99999999', 
            '12345678', 
        ]
        for item in data: 
            self.assertEqual(check_empty(item), item)

    def test_check_empty_valid_strip_cases(self):
        data = [
            '\nasda',
            ' a',
        ]
        for item in data: 
            self.assertEqual(check_empty(item), item.strip())


if __name__ == '__main__':
    unittest.main()