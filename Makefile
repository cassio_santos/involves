migration:
	curl -v -X POST \
	http://172.19.0.3:5000/_migrations

tenant:
	curl -v -X POST \
	http://172.19.0.3:5000/tenant \
	-H 'cache-control: no-cache' \
	-H 'content-type: multipart/form-data' \
	-F tenant=cassio \
	-F tenant_data_source='{"host": "db", "user": "postgres", "password": "123456", "dbname": "cassio"}'

test:
	curl -v -X POST \
	http://172.19.0.3:5000/ \
	-H 'cache-control: no-cache' \
	-H 'content-type: multipart/form-data' \
	-F data=@tests/pdvs.csv \
	-F tenant=cassio

read:
	curl --silent \
	http://172.19.0.3:5000/cassio \
	-H 'cache-control: no-cache' | jq

